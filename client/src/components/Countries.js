import React, { useEffect, useState } from "react";
import * as d3 from "d3";
import { call, tickFormat, select, selectAll, remove } from "d3";
import countries from "./countries.csv";
import "../App.css";

const Countries = () => {
  //const [data, setData] = useState();
  const [data, setData] = useState({ title: "Most Populous Countries" });
  useEffect(() => {
    const svg = d3.select(".graph");
    const width = +svg.attr("width");
    const height = +svg.attr("height");
    const render = reactData => {
      const xValue = d => d.population;
      const yValue = d => d.country;
      const margin = { top: 80, right: 20, bottom: 100, left: 200 };
      const innerWidth = width - margin.left - margin.right;
      const innerHeight = height - margin.bottom - margin.top;
      const xScale = d3
        .scaleLinear()
        .domain([0, d3.max(reactData, d => xValue(d))])
        .range([0, innerWidth]);

      //console.log(xScale.range());

      const yScale = d3
        .scaleBand()
        .domain(reactData.map(d => yValue(d)))
        .range([0, innerHeight])
        .padding(0.2);

      const xAxisTickFormat = number =>
        d3
          .format(".3s")(number)
          .replace("G", "B");

      const yAxis = d3.axisLeft(yScale);

      const xAxis = d3
        .axisBottom(xScale)
        .tickFormat(xAxisTickFormat)
        .tickSize(-innerHeight);

      const g = svg
        .append("g")
        .attr("transform", `translate(${margin.left},${margin.top})`);

      g.append("g")
        .call(yAxis)
        .selectAll(".domain, .tick line")
        .remove();

      const xAxisG = g
        .append("g")
        .call(xAxis)
        .attr("transform", `translate(0, ${innerHeight})`);

      xAxisG.select(".domain").remove();

      g.append("text")
        .attr("y", -20)
        .text(data.title);

      xAxisG
        .append("text")
        .attr("x", innerWidth / 2)
        .attr("y", 60)
        .attr("fill", "black")
        .text("Population");

      //console.log(yScale.domain());
      g.selectAll("rect")
        .data(reactData)
        .enter()
        .append("rect")
        .attr("y", d => yScale(yValue(d)))
        .attr("width", d => xScale(xValue(d)))
        .attr("height", yScale.bandwidth());
    };

    d3.csv(countries).then(countries => {
      countries.forEach(d => {
        d.population = +d.population * 1000;
      });
      render(countries);
      //setData(countries);
    });
  }, []);
  return (
    <div>
      <svg className='graph' width='1100' height='500'></svg>
    </div>
  );
};

export default Countries;
