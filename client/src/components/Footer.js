import React from "react";

const Footer = () => {
  return (
    <div>
      <p>
        This bar chart represents the top 10 most populous countries. The data
        comes from the 2018 estimate in{" "}
        <a href='https://population.un.org/wpp/Download/Standard/Population/'>
          United Nations: Word Population Prospect
        </a>
      </p>
    </div>
  );
};

export default Footer;
