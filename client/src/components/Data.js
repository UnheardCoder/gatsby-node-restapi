import React, { useEffect, useState } from "react";
import axios from "axios";
axios.defaults.proxy.host = "http://localhost:5000";

const Data = () => {
  const [data, setData] = useState([]);
  useEffect(() => {
    const getData = async () => {
      const realData = await axios.get("/data");
      const seriesOne = [];
      const getArrData = () =>
        realData.data.demo.map(item => {
          seriesOne.push(Object.values(item.data));
        });
      getArrData();
      setData(seriesOne);
      //console.log(seriesOne)
    };
    getData();
  }, []);
  //console.log(data)
  return (
    <>
      <ul>
        {data.map((item, i) => (
          <li key={i}>
            <p>{item}</p>
          </li>
        ))}
      </ul>
    </>
  );
};

export default Data;
