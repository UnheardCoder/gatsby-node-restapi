import React, { useEffect, useState } from "react";
import axios from "axios";

const TestGraph = () => {
  const [dataG, setDataG] = useState();
  const [url, setUrl] = useState("http://localhost:5000/data");
  useEffect(() => {
    const fetchData = async () => {
      const result = await axios(url);
      const graphData = result.data;
      console.log(graphData);
    };
    fetchData();

    const render = () => {};
  }, [url]);
  return (
    <div>
      <svg width='750' height='500'></svg>
    </div>
  );
};

export default TestGraph;
