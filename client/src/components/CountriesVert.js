import React, { useEffect, useState } from "react";
import * as d3 from "d3";
import { call } from "d3";
import countries from "./countries.csv";
import "../App.css";

const CountriesVert = () => {
  const [data, setData] = useState({ title: "Most Populous Countries" });
  useEffect(() => {
    const svg = d3.select(".graphVert");
    const width = +svg.attr("width");
    const height = +svg.attr("height");
    const render = dataVert => {
      const xValue = d => d.country;
      const yValue = d => d.population;
      const margin = { top: 0, right: 10, bottom: 50, left: 100 };
      const innerWidth = width - margin.left - margin.right;
      const innerHeight = height - margin.bottom - margin.top;

      const xScale = d3
        .scaleBand()
        .domain(dataVert.map(d => xValue(d)))
        .range([innerWidth, 0])
        .padding(0.3);

      const yScale = d3
        .scaleLinear()
        .domain([0, d3.max(dataVert, d => yValue(d))])
        .range([innerHeight, 0]);
      //.padding(0.2);

      const xAxisTickFormat = number =>
        d3
          .format(".3s")(number)
          .replace("G", "B");

      const yAxis = d3
        .axisLeft(yScale)
        .tickFormat(xAxisTickFormat)
        .tickSize(-innerWidth);

      const xAxis = d3.axisBottom(xScale);

      const g = svg
        .append("g")
        .attr("transform", `translate(${margin.left},${margin.top})`);

      //yAxis(g.append("g"));
      const yAxisG = g.append("g").call(yAxis);
      const xAxisG = g
        .append("g")
        .call(xAxis)
        .attr("transform", `translate(0, ${innerHeight})`);

      yAxisG.select(".domain").remove();
      xAxisG.selectAll(".domain").remove();
      //xAxisG.select

      // g.append("text")
      //   .attr("y" - 20)
      //   .text(data.title);

      //console.log(yScale.domain());
      g.selectAll("rect")
        .data(dataVert)
        .enter()
        .append("rect")
        .attr("x", d => xScale(xValue(d)))
        .attr("y", d => innerHeight - xScale(xValue(d)))
        .attr("width", xScale.bandwidth())
        .attr("height", d => xScale(xValue(d)));
    };

    d3.csv(countries).then(countries => {
      countries.forEach(d => {
        d.population = +d.population * 1000;
      });
      render(countries);
    });
  }, []);

  return (
    <div>
      <svg className='graphVert' width='1300' height='1200'></svg>
    </div>
  );
};

export default CountriesVert;
