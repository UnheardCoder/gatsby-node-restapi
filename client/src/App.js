import React from "react";
import "./App.css";
import Countries from "./components/Countries";
import CountriesVert from "./components/CountriesVert";
import Footer from "./components/Footer";
import TestGraph from "./components/TestGraph";

function App() {
  return (
    <>
      <Countries />
      <CountriesVert />
      <TestGraph />
      <Footer />
    </>
  );
}

export default App;
